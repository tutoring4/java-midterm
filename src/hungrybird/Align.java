package hungrybird;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Align {
  public Align() {}

  public Stage run(Stage primaryStage) {
    // for the left panel
    Text text1 = new Text(100, 100, "Snap to Grid");
    Text text2 = new Text(100, 120, "Show Grid");

    CheckBox checkBox1 = new CheckBox("Snap to Grid");
    CheckBox checkBox2 = new CheckBox("Show Grid");

    // for the center panel
    Label label1 = new Label("X:");
    TextField textField1 = new TextField ();
    Label label2 = new Label("Y:");
    TextField textField2 = new TextField ();

    // for then right panel
    Button btn1 = new Button("OK");
    Button btn2 = new Button("Cancel");
    Button btn3 = new Button("Help");

    btn1.setMaxSize(100, 200);
    btn2.setMaxSize(100, 200);
    btn3.setMaxSize(100, 200);

    // button event handler. users input will be printed upon ok button click event.
    btn1.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        System.out.println("x: " + textField1.getText());
        System.out.println("y: " + textField1.getText());
        System.out.println("Snap to Grid: " + checkBox1.isSelected());
        System.out.println("Show Grid: " + checkBox2.isSelected());
      }
    });

    btn2.setOnAction((event -> {
      System.out.println("Cancel");
    }));

    btn3.setOnAction((event -> {
      System.out.println("Help");
    }));

    // adding the gridpane properties
    GridPane gridPane = new GridPane();
    gridPane.setPadding(new Insets(10, 10, 10, 90));
    gridPane.setVgap(10);
    gridPane.setHgap(10);

    // setting them their positions
    GridPane.setConstraints(checkBox1, 0, 0);
    GridPane.setConstraints(checkBox2, 0, 1);

    GridPane.setConstraints(label1, 1, 0);
    GridPane.setConstraints(textField1, 2, 0);

    GridPane.setConstraints(label2, 1, 1);
    GridPane.setConstraints(textField2, 2, 1);

    GridPane.setConstraints(btn1, 3, 0);
    GridPane.setConstraints(btn2, 3, 1);
    GridPane.setConstraints(btn3, 3, 2);

    // adding them into gridpane
    gridPane.getChildren().addAll(checkBox1, checkBox2, label1, textField1, label2, textField2, btn1, btn2, btn3);
    Scene scene = new Scene(gridPane, 500, 300, Color.WHITE);

    primaryStage.setTitle("Align");
    primaryStage.setScene(scene);
    return primaryStage;
  }
}
