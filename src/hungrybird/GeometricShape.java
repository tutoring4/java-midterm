package hungrybird;

public class GeometricShape {
  private String colour = "white";
  private boolean filled = false;
  private Date dateCreated;

  public GeometricShape() {
    System.out.println("Calling GeometricShape no-arg Constructor!");
    this.dateCreated = new Date();
  }

  public GeometricShape(String colour, boolean filled) {
    System.out.println("Calling GeometricShape paramters Constructor!");
    this.colour = colour;
    this.filled = filled;
    this.dateCreated = new Date();
  }

  public String getColour() {
    return colour;
  }

  public void setColour(String colour) {
    this.colour = colour;
  }

  public boolean isFilled() {
    return filled;
  }

  public void setFilled(boolean filled) {
    this.filled = filled;
  }

  public Date getDateCreated() {
    return dateCreated;
  }

  @Override
  public String toString() {
    return "Colour: " + getColour() + 
      "\nFilled: " + isFilled() + 
      "\nDate: " + getDateCreated();
  }
}
