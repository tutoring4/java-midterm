package hungrybird;

import java.util.Scanner;
import javafx.application.Application;
import javafx.stage.Stage;

// has to extend from Application javafx class to use GUI
public class Main extends Application {

    // used to prompt inputs for rectangle object and return its newly created object
    public static Rectangle prompt() {
        System.out.println("###### input rectangle inputs ######");
        Scanner scanner = new Scanner(System.in);
        System.out.print("what is width? ");
        double w = scanner.nextDouble();
        System.out.print("what is height? ");
        double h = scanner.nextDouble();
        System.out.print("what is color? ");
        scanner.nextLine();
        String color = scanner.nextLine();
        System.out.print("is it filled? (yes/no) ");
        boolean filled = scanner.nextLine().matches("yes|Yes|y|Y|1");

        return new Rectangle(w, h, color, filled);
    }

    public static void main(String[] args) {
//// Q1
        MyArray myArray = new MyArray();
        myArray.run();

//// Q2
        Rectangle rectangle1 = prompt();
        Rectangle rectangle2 = prompt();
        Rectangle rectangle3 = prompt();
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle3);

//// Q3
        // it launches application of javafx
        launch(args);
    }

    // method that I have to override from Application class to start GUI app
    @Override
    public void start(Stage primaryStage) throws Exception {
        new Align().run(primaryStage).show();
    }
}
