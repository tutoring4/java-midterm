package hungrybird;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MyArray {
  private final Scanner scanner = new Scanner(System.in);
  private int[] inputs;

  public void run() {
    System.out.print("Enter the integers between 1 and 1000: ");
    String inputs = scanner.nextLine();

    // the input is taken as string. it splits into an array of string. then parse each element into int.
    this.inputs = Arrays.stream(inputs.split(" ")).mapToInt(Integer::parseInt).toArray();
    this.insertionSort();
    print();
  }

  // insertion sort used to sort the input array
  private void insertionSort() {
    int n = this.inputs.length;
    for(int i=0; i<n; i++) {
      int key = this.inputs[i];
      int j = i - 1;

      while(j >= 0 && this.inputs[j] > key) {
        this.inputs[j + 1] = this.inputs[j];
        j--;
      }
      this.inputs[j+1] = key;
    }
  }

  private void print() {
    List<Integer> checked = new ArrayList<>();

    for(int i = 1; i<this.inputs.length; i++) {
      // see if checked list already contains the value. if yes, and we skip the current iteration
      if(checked.contains(this.inputs[i])) {
        continue;
      }
      int occurrences = 1;
      // if current loop is in the last iteration, we just sprint
      if(i == this.inputs.length-1) {
        System.out.printf("%d occurs 1 time\n", this.inputs[i]);
        return;
      }

      // check how many occurrences can be found by using while loop.
      int j = i + 1;
      while(this.inputs[j] == this.inputs[i]) {
        occurrences++;
        j++;
        // this is the case for inputs having more than 1 occurrences at the end of inputs
        // we subtract by 1 and break the loop to avoid index exception and that would the occurrences of last input value
        if (j == this.inputs.length) {
          j--;
          break;
        }
      }
      // used ternary operator to print time or times depending on the value of occurrences
      System.out.printf("%d occurs %d time%s\n", this.inputs[i], occurrences, occurrences == 1 ? "" : "s");
      checked.add(this.inputs[i]);
    }
  }
}
