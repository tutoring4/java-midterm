package hungrybird;

public class Rectangle extends GeometricShape {
  private double w;
  private double h;

  // default constructor which calls the super class default constructor
  public Rectangle() {
    super();
    this.w = 3.0;
    this.h = 4.0;
  }

  public Rectangle(double w, double h, String color, boolean filled) {
    super(color, filled);
    this.w = w;
    this.h = h;
  }

  // super class toString is called and combined with those parameters that exist only in this class
  @Override
  public String toString() {
    String s = super.toString();
    s += String.format(", color = %s and filled = %s\n", this.color, this.filled ? "Yes" : "No");
    s += String.format("parameter = %.1f and area = %.1f", super.getPerimeter(), super.getArea());
    return s;
  }
}